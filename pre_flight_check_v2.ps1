#-----------------------------------------------------------------------#
# Create User Information for tasks
#-----------------------------------------------------------------------#

#-----------------------------------------------------------------------#
# Configure PS Cred Pop Ups
#-----------------------------------------------------------------------#
Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\PowerShell\1\ShellIds" -Name ConsolePrompting -Value $true

# Get Users credentials
$psCredAdmin = Get-Credential

if ($psCredAdmin -eq $null) 
{
	Exit
}

# Remove any leading slashes
$tempUsername = $psCredAdmin.UserName -creplace '^[^\\]*\\',''

# Create new credential to be used for domain join
$scriptAdmin =  New-Object System.Management.Automation.PSCredential($tempUsername,$psCredAdmin.Password)

# validate $scriptAdmin password
Add-Type -Path ".\System.DirectoryServices.AccountManagement.dll"
try 
{ 
	Add-Type -AssemblyName System.DirectoryServices -ErrorAction stop 
} 
catch [system.exception]
{
	write-host "Could not load System.DirectoryServices Assembly".
}
	
$domain = "EAD"
$cndomain = "DC=EAD,DC=UBC,DC=CA"
$ct = [System.DirectoryServices.AccountManagement.ContextType]::Domain
$pc = New-Object System.DirectoryServices.AccountManagement.PrincipalContext($ct, $domain, $cndomain)

if ($pc.ValidateCredentials($scriptAdmin.username,$scriptAdmin.GetNetworkCredential().password) -eq $false)
{
	Write-Host "Invalid Username or Password"
	Exit
}
else
{
	Write-host "Username and Password for Domain Join Validated."
}

#-----------------------------------------------------------------------#
# Get Files
#-----------------------------------------------------------------------#

$preflightFileHTTP = "https://b-svn1.bis.it.ubc.ca/svn/powershell/Branch01/Preflight-DEV/Preflight.7z"

if (!($preflightFileLocal = gci ".\Preflight.7z" -ErrorAction SilentlyContinue))
{
	Write-Host "Local Preflight.7z not found. Downloading $preflightFileHTTP...."
	$processSVN = Start-Process -FilePath .\svn.exe -ArgumentList @("export",$preflightFileHTTP,"--force") -NoNewWindow -PassThru -wait
	$preflightFileLocal = gci "Preflight.7z"
	$process7z = Start-Process -FilePath .\7za.exe -ArgumentList @("x",$preflightFileLocal.FullName,"-y") -NoNewWindow -PassThru -wait
}
else
{
	$process7z = Start-Process -FilePath .\7za.exe -ArgumentList @("x",$preflightFileLocal.FullName,"-y") -NoNewWindow -PassThru -wait
}

#-----------------------------------------------------------------------#
# Load Configs
#-----------------------------------------------------------------------#

[xml]$config =  gc $PSScriptRoot\preflightconfig_v2.xml
$folders = $config.config.folders.name
$log = $config.config.log
$network = $config.config.network
$software = $config.config.software
$accounts = $config.config.accounts
$nsclient = $software.nsclient
$7zip = $software.Sevenzip
$notepadpp = $software.notepadpp
$sophos = $software.sophos
#$lemss = $software.lemss
$powershell = $software.powershell
$bginfo = $software.bginfo

#-----------------------------------------------------------------------#
# Perform Security Settings and Configs
#-----------------------------------------------------------------------#

# Disable IEES for Admins
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}" -Name "IsInstalled" -Value 0
& rundll32 iesetup.dll,IEHardenAdmin
& rundll32 iesetup.dll,IEHardenMachineNow

# Enable PSRemoting
REG ADD HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce /v WinRMHTTPS /t REG_SZ /d "winrm quickconfig -quiet -transport:https"
enable-psremoting -force

# WMI objects
$networkAdapters = gwmi Win32_NetworkAdapterConfiguration -filter "IPEnabled=TRUE"
$nacBind = [wmiclass]"Win32_NetworkAdapterConfiguration"

# Disable UAC
write-host "Disabling UAC"
Set-ItemProperty -Path registry::HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\policies\system -Name EnableLUA -Value 0

#-----------------------------------------------------------------------#
# Create Folder and File Structres
#-----------------------------------------------------------------------#

# Create Folders
$folders | %{
	if ((test-path $_) -eq $false) {
		new-item -Path $_ -type directory
	} else {
		Write-Host "$_ exists."
	}
}

if ((test-path $log.file) -eq $false) {
	if (new-item -Path $log.file -type file) {
		$create_log = ".LOG"
		$create_log > $log.file
		C:\Windows\System32\cmd /c mklink $log.shortcut $log.file
	}
} else { 
	Write-Host "Could not create log file."
}

#-----------------------------------------------------------------------#
# Install Software
#-----------------------------------------------------------------------#

write-host "Preparing to install software"

# Extract Zip Archives
write-host "Extracting Install Files"


# Install NSCP
write-host "Installing $($nsclient.filename)"
start-process -filepath $nsclient.filename -argumentlist $nsclient.arguments -wait
write-host "Stopping NSClient++ service"
stop-service nscp
write-host "Copying over NSClient++ config file"
$nsclientConfig = $nsclient.configfile
copy-item $nsclientConfig "C:\Program Files\NSClient++\$nsclientConfig" -force
write-host "Starting NSClient++ service"
start-service nscp

# Install 7zip
write-host "Installing $($7zip.filename)"
start-process -filepath $7zip.filename -argumentlist $7zip.arguments -wait

# Install NotePad++
write-host "Installing $($notepadpp.filename)"
start-process -filepath $notepadpp.filename -argumentlist $notepadpp.arguments -wait

# Install Sophos
write-host "Installing $($sophos.filename)"
start-process -filepath $sophos.filename -argumentlist $sophos.arguments -wait

# Install Lemss
# if ((gwmi -Query "select OSArchitecture from win32_operatingsystem").osarchitecture -eq "64-bit") { 
	# write-host "Installing $($lemss.filename.x64)"
	# start-process -filepath $($lemss.filename.x64) -argumentlist $lemss.arguments -wait
# } else {
	# write-host "Installing $($lemss.filename.x86)"
	# start-process -filepath $($lemss.filename.x86) -argumentlist $lemss.arguments -wait
# }

# Install BGINFO
$bginfoFilename = $bginfo.filename
$bginfoFoldername = $bginfo.foldername
$bginfoShortcut = $bginfo.shortcut
$bginfoWorkingDir = $bginfo.workingdir
if (!(test-path "C:\Program Files\Bginfo")) {
	cp ".\$bginfoFoldername" $bginfoFilename -recurse -force -verbose
}
if (test-path "C:\Program Files\Bginfo") {
	$wshShell =  New-Object -ComObject WScript.Shell
	$shortcut = $wshShell.CreateShortcut($bginfoShortcut)
	$shortcut.TargetPath = $bginfoFilename
	$shortcut.Arguments = "vss.bgi /NOLICPROMPT /SILENT /TIMER:0"
	$shortcut.Description = "Bginfo Application"
	$shortcut.WorkingDirectory = $bginfoWorkingDir
	$shortcut.Save()
} else {
	write-warning "'C:\Program Files\Bginfo' Not Found!  Not Link Created."
}

# Install Powershell
$powershellVersion = $powershell.version
if (!($PSVersionTable.PSVersion.major -ge $powershellVersion)) {
	write-host "Current Powershell major version is $($PSVersionTable.PSVersion.major)"
	write-host "Installing Powershell version $powershellVersion"
	start-process -filepath $powershell.filename -argumentlist $powershell.arguments -wait
}

# Install PortQry
write-host "Installing PortQryV2"
cp ".\PortQry.exe" "C:\DRS\Software" -force -verbose
$path = (Get-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment" -Name PATH).path
$path += ";C:\DRS\Software\"
Set-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment" -Name PATH -Value $path

# Install .NET framework 3.5 on Windows 2012 server
$os = gwmi -class win32_operatingsystem -Property caption | select -exp caption
if ($os.Contains("Microsoft Windows Server 2012")) {
	write-host "Installing .NET FRAMEWORK 3.5"
	Install-WindowsFeature Net-Framework-Core -source $PSScriptRoot\sxs
}

#-----------------------------------------------------------------------#
# Configure Network and Firewall Settings
#-----------------------------------------------------------------------#

# Configure Network Adapters
write-host "Updating DNS Server Entries"
$dssoResult = $networkAdapters.SetDNSServerSearchOrder($network.dns.server)
$dssoResult.ReturnValue
write-host "Updating DNS Suffix Search Order"
[array]$suffixes = $network.dns.suffix
$dssoResult = $nacBind.SetDNSSuffixSearchOrder($suffixes)
$dssoResult.ReturnValue

# Configure Firewall
write-host "Adding Firewall rule for ICMP (PING)"
netsh advfirewall firewall add rule name="icmp" dir=in action=allow enable=yes protocol=icmpv4 interfacetype=any profile=any
if ($network.firewall.disable -eq 1) {
	write-host "Disabling Advanced Firewall on all Profiles"
	netsh advfirewall set allprofiles state off
}

if ($network.firewall.disable -eq 0) {
	$netsh = "C:\Windows\system32\netsh.exe"

	$network.firewall.rule | %{
		Invoke-Expression $_
		#start-process -filepath $netsh -argumentlist $_ -wait
	}
}

#-----------------------------------------------------------------------#
# Join Domain and Restart Machine
#-----------------------------------------------------------------------#

# Join Domain
write-host "Joining computer to $($network.dns.dnsdomain)"
add-computer -domainname $network.dns.dnsdomain -credential $psCredAdmin -OUPath $network.ad.ou

#-----------------------------------------------------------------------#
# Configure Administrator accounts and Groups
#-----------------------------------------------------------------------#

# Configure Local Admin Account
write-host "Changing Local Administrator Account"
$computername = (gci env:computername).value
$localAdmin = [ADSI]("WinNT://$computername/Administrator")
$localAdmin
do {
	$adminPassword1 = read-host "Enter New Admin Password" -AsSecureString
	$adminPassword2 = read-host "Enter New Admin Password again" -AsSecureString
	$tmpA = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($adminPassword1))
	$tmpB = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($adminPassword2))
} while ($tmpA -ne $tmpB)
write-host "Setting Local Administrator Account Password"
$localAdmin.SetPassword([Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($adminPassword1)))
$tmpA = $null
$tmpB = $null
write-host "Renaming Local Administrator Account to $($accounts.adminaccount.name)"
$localAdmin.psbase.rename($accounts.adminaccount.name)

# Add BIS Admin group to Administrators and remove Domain Admins from Administrators Group.
$administratorsGroup = [ADSI]("WinNT://./Administrators")
$eadBISAdminGroupadspath = "WinNT://ead.ubc.ca/BIS-AOP-DelegationFC"
$eadBISAdminGroup = New-Object System.DirectoryServices.DirectoryEntry($eadBISAdminGroupadspath,$scriptAdmin.Username,$scriptAdmin.GetNetworkCredential().password)
$administratorsGroup.psbase.invoke("Add",$eadBISAdminGroup.psbase.path)
$eadDomainAdminsGroupadspath = "WinNT://ead.ubc.ca/Domain Admins"
$eadDomainAdminsGroup = New-Object System.DirectoryServices.DirectoryEntry($eadDomainAdminsGroupadspath,$scriptAdmin.Username,$scriptAdmin.GetNetworkCredential().password)
$administratorsGroup.psbase.invoke("Remove",$eadDomainAdminsGroup.psbase.path)

#-----------------------------------------------------------------------#
# Update Powershell to x.0
#-----------------------------------------------------------------------#
# Update Powershell
write-host "Updating Powershell to version $($powershell.version)"
Start-Process -FilePath $powershell.filename -ArgumentList $powershell.arguments -wait

write-host "PLEASE RESTART MACHINE!" -foregroundcolor RED
restart-computer -confirm
# SIG # Begin signature block
# MIIITQYJKoZIhvcNAQcCoIIIPjCCCDoCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQU15fW1FYBJyoMDFMeEbnh+lmL
# lEqgggW9MIIFuTCCBKGgAwIBAgIKYShfEAAAAAPEWTANBgkqhkiG9w0BAQUFADBL
# MRIwEAYKCZImiZPyLGQBGRYCY2ExEzARBgoJkiaJk/IsZAEZFgN1YmMxEzARBgoJ
# kiaJk/IsZAEZFgNlYWQxCzAJBgNVBAMTAkNBMB4XDTE1MDQyMjE3MzA0M1oXDTIw
# MDQyMjE3NDA0M1owcDESMBAGCgmSJomT8ixkARkWAmNhMRMwEQYKCZImiZPyLGQB
# GRYDdWJjMRMwEQYKCZImiZPyLGQBGRYDZWFkMQ8wDQYDVQQLEwZBRE1JTlMxHzAd
# BgNVBAMTFlBhbCwgTWljaGFlbCAobXBhZG1pbikwggEiMA0GCSqGSIb3DQEBAQUA
# A4IBDwAwggEKAoIBAQCU4jjMBgWPw/QXEZlOctOHJf6gR80C1UXcbe7Thj2TGeCV
# wy/2Hs6ouiKGGvgPP/u2AUaA7/tP1CMW8kb4LdXYxEliO8eiKNrYgbLq3jBJAmLj
# AHe9+PQe4MTwPtFOjHzWxrWvJnXqUWUJQpjjNOJCl9BpaJkwiJ//ejV2KTJcchs/
# BeIxFnwXuspapSS/gC94ZZpK/QcIdIwgoToiMgj1jM+SVilctgyxEO+B8F/uyVTq
# VurdYxJBwRIEeWz2ubXULBNQHENMTT6OmWweHEDv5Nn7caAYdS/3+nX4LbYsI0RJ
# /IPpLZDk6NRWbRuPDdu3fWYbG5Fde747pqmDrHrpAgMBAAGjggJ4MIICdDA9Bgkr
# BgEEAYI3FQcEMDAuBiYrBgEEAYI3FQiDo/srheKaPYXZlwGCzINshaSJBCSHy900
# g/LsPAIBZAIBAzATBgNVHSUEDDAKBggrBgEFBQcDAzAOBgNVHQ8BAf8EBAMCB4Aw
# GwYJKwYBBAGCNxUKBA4wDDAKBggrBgEFBQcDAzAdBgNVHQ4EFgQUUds9yQ3SJT/U
# 5rNv72RP2TH0AXcwHwYDVR0jBBgwFoAUAc1YSIa2HIFsHqsSyC7GxAdC7NAwgcgG
# A1UdHwSBwDCBvTCBuqCBt6CBtIaBsWxkYXA6Ly8vQ049Q0EsQ049Uy1JVFNWLUNS
# VDAxUCxDTj1DRFAsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2Vydmlj
# ZXMsQ049Q29uZmlndXJhdGlvbixEQz1lYWQsREM9dWJjLERDPWNhP2NlcnRpZmlj
# YXRlUmV2b2NhdGlvbkxpc3Q/YmFzZT9vYmplY3RDbGFzcz1jUkxEaXN0cmlidXRp
# b25Qb2ludDCBtgYIKwYBBQUHAQEEgakwgaYwgaMGCCsGAQUFBzAChoGWbGRhcDov
# Ly9DTj1DQSxDTj1BSUEsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2Vy
# dmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1lYWQsREM9dWJjLERDPWNhP2NBQ2Vy
# dGlmaWNhdGU/YmFzZT9vYmplY3RDbGFzcz1jZXJ0aWZpY2F0aW9uQXV0aG9yaXR5
# MC0GA1UdEQQmMCSgIgYKKwYBBAGCNxQCA6AUDBJtcGFkbWluQGVhZC51YmMuY2Ew
# DQYJKoZIhvcNAQEFBQADggEBAJrSJoMPsks2qev7i/RJ1lJtClP2z99h+55Iigm/
# HiHjw6Fv+2VChQ8rfO1+qeb3rdl1/jqeVVcbSB/F6gJIafUMEHx6iQsZBIkGo2jA
# r/5i+KzfPwDXd4TooI/YILEUxdknoL0ZEX7RLUpqoXADC8aYSQZhs/zBOtaiKrTy
# MfFo69l9F+pQaCrqVxfugT+S5nwxCMQckC+VLTH+eykHYRGkZ+76LhTytJDMdQIR
# tPUdM+c6btBHEzG/Su/vn39qNqaHtdE6R88W++ZXZLy0G998iSzcJ9MltXSD6PN4
# nhv1UlCPW1wZtWU2NwEtRcuNhdXKD5kHH3Ijw8mx0o13x9oxggH6MIIB9gIBATBZ
# MEsxEjAQBgoJkiaJk/IsZAEZFgJjYTETMBEGCgmSJomT8ixkARkWA3ViYzETMBEG
# CgmSJomT8ixkARkWA2VhZDELMAkGA1UEAxMCQ0ECCmEoXxAAAAADxFkwCQYFKw4D
# AhoFAKB4MBgGCisGAQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJKoZIhvcNAQkDMQwG
# CisGAQQBgjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcCARUwIwYJKoZI
# hvcNAQkEMRYEFK062ROdyDrL/W3gKRHBpEW9FGhTMA0GCSqGSIb3DQEBAQUABIIB
# AII1B7P5levj1HpCGlSirnnUo90ySOF8vwxODAwGA4qUc8JmWgWMCdJ7VG7LWo+S
# sXOYBQsWMSrSnQ1y9rEWN8DJ4XZumToAXjAGbOKKQIHq6cjWCA8rYXeoUn3+N8zJ
# 68rjhFcf16z8W+K7gIpumgZ/lkhhYmwwAlS8gYq1wnBaTJIM30lvpm6rV6C3+H4y
# rXi/W7vsRw2K76AilrC14LzZ58TEDNNRzKG60EKE2CQnqaryIlcabWUXV5/DEbPX
# VPC0YpRIzkcIyYPHybwgJzd11W2/ZYbNzR+8YdJqdiT/V1lIp69uk05a9M8pmTyV
# wM47Yh4/+vMEG51gd1S9mHo=
# SIG # End signature block
